# Car example

The *Car* example demonstrates how to translate the UML model into C++ classes and express associative links by tokens from [C++ Object Token Library](https://gitlab.com/CppObjectTokens/Module/Library/CppOtl).

## Class diagram

![CarClassDiagram](./doc/image/CarClassDiagram.png)

## C++ classes

### Printable

```c++
class Printable
{
public:
    virtual void print() const = 0;
    ...
};
```


### Engine

```c++
class Engine : public Printable
{
public:
    ...
    void accelerate();
    int  currentRpm() const;

    void print() const override;
    ...

private:
    ...
};
```

### Car

```c++
class Car : public Printable
{
public:
    using SingleEngine = otn::unique_single<Engine>;

    Car(const std::string& brand, SingleEngine engine)
        : m_brand{brand}, m_engine{std::move(engine)}
    {}

    SingleEngine replaceEngine(SingleEngine engine)
    {
        using std::swap;
        swap(m_engine, engine);
        return engine;
    }

    void drive() { (*m_engine).accelerate(); }

    void print() const override;

    ...

private:
    std::string  m_brand;
    SingleEngine m_engine;
};
```

### Monitor

```c++
class Monitor : public Printable
{
public:
    using WeakEngine = otn::weak_optional<const Engine>;

    Monitor() {}
    Monitor(const std::string& model, const WeakEngine& engine)
        : m_model{model}, m_engine{engine}
    {}

    std::string model() const { return m_model; }

    void setEngine(const WeakEngine& engine)
    { m_engine = engine; }

    void print() const override;

    ...

private:
    std::string m_model;
    WeakEngine  m_engine;
};
```

## Building the example

The *Car* example is part of the [Example](https://gitlab.com/CppObjectTokens/Complex/Example) complex and is building with it.
