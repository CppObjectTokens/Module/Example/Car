/*
 * Copyright (c) 2018-2020 Viktor Kireev
 * Distributed under the MIT License
 */

#include <otn/all.hpp>

#include <thread>
#include <queue>
#include <iostream>

#define THREAD_ID "[" << std::this_thread::get_id() << "] "

class Printable
{
public:
    virtual ~Printable() = default;

    virtual void print() const = 0;
};

template <class Token>
std::enable_if_t<std::is_base_of_v<Printable, otn::traits::element_t<Token>>>
print(const Token& token)
{
#if 1
    // Access to the 'token' by the lambda.
    otn::access(token, [](auto& value)
                { value.print(); },
                [] // else
                { std::cout << "no value" << std::endl; });
#else
    // Similar access to the 'token' by the "classic" way.
    if (auto loc = otn::gain(token))
    {
        auto& value = *loc;
        value.print();
    }
    else
    {
        std::cout << "no value" << std::endl;
    }

#endif
}

class Engine : public Printable
{
public:
    Engine() = default;
    Engine(const std::string& model)
        : m_model{model}
    {}

    Engine(const Engine& other) = delete;
    Engine&     operator=(const Engine& other) = delete;

    std::string model() const { return m_model; }
    int  currentRpm() const   { return m_current_rpm; }

    void accelerate() { m_current_rpm++; }

    void print() const override
    {
        std::cout << THREAD_ID
                  << "engine[" << model()
                  << "]" << std::endl;
    }

private:
    std::string m_model;
    int m_current_rpm{0};
};

class Car : public Printable
{
public:
    using SingleEngine = otn::unique_single<Engine>;

    Car(const std::string& brand, SingleEngine engine)
        : m_brand{brand}, m_engine{std::move(engine)}
    {}

    SingleEngine replaceEngine(SingleEngine engine)
    {
        using std::swap;
        swap(m_engine, engine);
        return engine;
    }

    void drive() { (*m_engine).accelerate(); }

    std::string brand() const { return m_brand; }

    void print() const override
    {
        std::cout << THREAD_ID
                  << "car[" << brand()
                  << ", engine[" << (*m_engine).model()
                  << "]]" << std::endl;
    }

private:
    std::string  m_brand;
    SingleEngine m_engine;
};

class Monitor : public Printable
{
public:
    using WeakEngine = otn::weak_optional<const Engine>;

    Monitor() {}
    Monitor(const std::string& model, const WeakEngine& engine)
        : m_model{model}, m_engine{engine}
    {}

    std::string model() const { return m_model; }

    void setEngine(const WeakEngine& engine)
    { m_engine = engine; }

    void print() const override
    {
        using namespace std;

        cout << THREAD_ID
             << "monitor[" << model()
             << "]" << endl;

        cout << "    ";
        // Print the 'otn::weak_optional<const Engine>' by the global 'print()'.
        ::print(m_engine);

        otn::access(m_engine, [](const auto& engine)
                    { cout << "    rpm = " << engine.currentRpm() << endl; });
    }

private:
    std::string m_model;
    WeakEngine  m_engine;
};

void driveCar(otn::weak_optional<Car> car)
{
    using namespace std::chrono_literals;

    for (int i = 0; i < 20; ++i)
    {
        std::this_thread::sleep_for(40ms);
        otn::access(car, [](auto& car) { car.drive(); });
    }
}

void monitorEngine(otn::weak_optional<const Monitor> monitor)
{
    using namespace std::chrono_literals;

    for (int i = 0; i < 10; ++i)
    {
        std::this_thread::sleep_for(100ms);
        otn::access(monitor, [](auto& monitor) { monitor.print(); });
    }
}

template <class Functor>
void addThread(std::vector<std::thread>& pool, Functor&& functor)
{
    using namespace std::chrono_literals;

    pool.push_back(std::thread{std::forward<Functor>(functor)});
    std::this_thread::sleep_for(10ms);
}

void execute(std::queue<std::function<void()>> commands)
{
    while (!commands.empty())
    {
        using namespace std::chrono_literals;

        std::this_thread::sleep_for(250ms);
        commands.front()();
        commands.pop();
    }
}

int main()
{
    // With the 'otn::itself' parameter, the 'otn::unique' creates the 'Engine'
    // in the same way as the 'std::make_unique<Engine>'.
    otn::unique_optional<Engine> vaz_i4{otn::itself, "VAZ I4"};
    otn::unique_optional<Engine> zmz_i4{otn::itself, "ZMZ I4"};

    otn::shared<Monitor> monitor_1{otn::itself, "ME 1", vaz_i4};
    otn::unique<Monitor> monitor_2{otn::itself, "ME 2", zmz_i4};

    otn::shared_single<Car>   lada{otn::itself, "LADA", std::move(vaz_i4)};
    otn::unique_optional<Car> uaz{otn::itself, "UAZ", std::move(zmz_i4)};

    std::vector<std::thread> threads;
    // Drive cars in separate threads.
    addThread(threads, [&] { driveCar(lada); });
    addThread(threads, [&] { driveCar(uaz); });
    // Monitor engines in separate threads.
    addThread(threads, [&] { monitorEngine(monitor_1); });
    addThread(threads, [&] { monitorEngine(monitor_2); });

    using EngineStore = otn::shared<std::vector<Car::SingleEngine>>;
    EngineStore engine_store{otn::itself};
    (*engine_store).emplace_back(otn::itself, "VAZ V4 Turbo");

    std::queue<std::function<void()>> commands;

    commands.emplace([ = ] // Capture the shared 'engine_store' and the 'lada' by value.
    {
        auto& store = (*engine_store);
        auto engine = (*lada).replaceEngine(std::move(store.back()));
        store.pop_back();
        store.push_back(std::move(engine));
    });

    commands.emplace( // Move the unique 'uaz' into the command with the 'otn::unique_carrier' helper.
        [uaz = otn::unique_carrier{std::move(uaz)}]() mutable
        { auto wall = std::move(uaz); });

    commands.emplace( // Capture the unique 'monitor_2' by the otn::weak_optional.
        [ =, m = otn::conform::weak(monitor_2)]
        { otn::access(m, [&](auto& m) { m.setEngine((*engine_store)[0]); }); });

    // Execute commands in a separate thread.
    addThread(threads, [&] { execute(std::move(commands)); });

    for (auto& thread:threads)
        thread.join();

    print(lada);
    print(uaz);

    return 0;
}
